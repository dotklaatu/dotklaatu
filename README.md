# dot-klaatu

My personal dot-config files for my Linux systems.

They probably aren't directly useful to anyone but me, but maybe
you'll find something interesting.

## dot-Xdefaults

For `rxvt-unicode`, including a solarised colour theme and some `perl`
generated tabs.

Save as `$HOME/.Xdefaults` and launch `urxvt` or whatever the binary
is called on your system.


## dot-Xmodmap

Replace **Caps_Lock** with a **Control** key. Set **Menu** to
**Compose**.

Save as `$HOME/.xmodmap` and place this in your `.bash_profile` or
`.bashrc`:

    xmodmap ~/.xmodmap


## dot-emacs

All my emacs configurations and packages and data. Lots of stuff I
stole shamelessly from Unix guru [William von Hagen](http://vonhagen.org).


## dot-fluxbox

All my Fluxbox configs, including menus and layout and styles. Save as
`.fluxbox` in your home directory.


## kconfig

Some KDE config files I use.


## dot-audacity-data

My custom key bindings for
[Audacity](http://audacity.sourceforge.net). Frankly, you shouldn't
use Audacity without this. It's just that good.


## pocketChip

Some helper scripts I use on my
[PocketChip](http://getchip.com). There's a dot-emacs file to make
Emacs more usable on the tiny PocketChip screen, and a Bluetooth
connection script that makes it so you don't have to type as much to
pair your PocketChip with a Bluetooth device (the PocketChip doesn't
ship with a Bluetooth GUI, so without this script you're fiddling
around with `bluetoothctl`).