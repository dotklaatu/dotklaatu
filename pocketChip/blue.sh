#!/bin/bash
# by Klaatu
# GPLv3 appears here

ARG="${1}"

help() {
    echo "Connect a bluetooth device to PocketCHIP"
    echo "Usage: $ [MOD=1] ./blue.sh BLUETOOTH_ID"
    echo "Optional: put your device ID into ~/.bluechip"
    exit 
}

xkbfunc() {
    setxkbmap dvorak
    xmodmap $HOME/Xmodmap.logitech
    exit 0
}

if [ -e ~/.bluechip ]; then
    ARG=`cat $HOME/.bluechip`
elif [ X"$ARG" = "X" ]; then
    help
fi

echo "Using ID $ARG"

# this is lazy
# TODO: check to see if bluetooth is running,
# respond accordingly
sudo systemctl start bluetooth || echo "Bluetooth already started or cannot be started."

sudo echo -e "power on\n connect $ARG \nquit" | bluetoothctl

## uncomment to turn on dvorak and Xmodmap
## I am commenting it out because bluetooth connection
## is unreliable, so I just do the dvorak and xmodmap
## steps manually after successful connection
#&& xkbfunc 

