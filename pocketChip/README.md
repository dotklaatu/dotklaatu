# PocketCHIP Scripts

These are some utils I use on my PocketCHIP.

The `blue.sh` script automates connecting a bluetooth device, with a
keyboard specifically in mind.

The Xmodmap is pretty specific to the Tablet Logitech keyboard that I
got for free from a friend. I don't know the model number. Keycodes
may differ for whatever you have.

The dot-emacs file makes Emacs a little nicer on the tiny little
PocketCHIP screen. I recommend launching emacs as `emacs -nw` and
using it in the terminal. The GUI, even modified, takes up too much
space.

Enjoy.